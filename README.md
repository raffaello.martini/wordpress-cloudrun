# Wordpress on Google Cloud Run
Cloned from https://github.com/johnsw87/wp-gcloud-run

## Prepare the environment
Create a CLoud SQL MYSQL instance

Remeber to create the wordpress database
```bash
INSTANCE_NAME='your_instance_name'
gcloud sql databases create wordpress -i ${INSTANCE_NAME}
```

Create a user
```bash
DB_USER='wp-user'
DB_PASSWORD='wp-pass'

gcloud sql users create ${DB_USER} \
--host=% \
--instance=${INSTANCE_NAME} \
--password=${DB_PASSWORD}
```

Create the Serverless VPC connector
```bash
CONNECTOR_NAME='wordpress'
REGION='europe-west1'
VPC_NETWORK='default'
IP_RANGE='172.16.255.0/28'
gcloud compute networks vpc-access connectors create ${CONNECTOR_NAME} \
--network ${VPC_NETWORK} \
--region ${REGION} \
--range ${IP_RANGE}
```

## Build and deploy

```bash
REGION=europe-west1
IMAGE_NAME=wordpress
CLOUD_SQL_CONNECTION_NAME='your_connection_name'
CONNECTOR_NAME=wordpress
VPC_CONNECTOR=projects/${GOOGLE_CLOUD_PROJECT}/locations/europe-west1/connectors/${CONNECTOR_NAME}
DB_HOST=":/cloudsql/${CLOUD_SQL_CONNECTION_NAME}"
DB_NAME='wordpress'

#build an image
gcloud builds submit --tag gcr.io/${GOOGLE_CLOUD_PROJECT}/${IMAGE_NAME}

#deploy to Cloud run
gcloud run deploy wordpress --region ${REGION}\
    --platform managed --image gcr.io/${GOOGLE_CLOUD_PROJECT}/${IMAGE_NAME}\
    --set-env-vars DB_NAME=$DB_NAME,DB_USER=$DB_USER,DB_PASSWORD=$DB_PASSWORD,DB_HOST=$DB_HOST\
    --port 80\
    --vpc-connector ${VPC_CONNECTOR}\
    --add-cloudsql-instances ${CLOUD_SQL_CONNECTION_NAME}\
    --allow-unauthenticated
```

Environment variables and port could be set via Cloud Run interface, or pass it via yaml file as `--env-vars-file .env.yaml` https://cloud.google.com/functions/docs/env-var

## Setup
- Dockerfile contains oficial PHP image with Apache and configuration for mysql connect and image handling.
- wp-config.php uses environment variables for database parameters instead of hard-coded values
- contains [WP-Stateless plugin](https://wordpress.org/plugins/wp-stateless/), which allow us to use Google Cloud Storage instead of local storage

## (optional) Save wp db password in Secret Manager
Create the secret
```bash
SECRET_NAME="wp-pass"
gcloud secrets create ${SECRET_NAME} \
    --replication-policy="automatic"
```

Generate a random pw and update the secret
```bash
echo -n $(openssl rand -base64 32) | gcloud secrets versions add ${SECRET_NAME} --data-file=-
```

(Optional) To access the secret use this command:
```bash
gcloud secrets versions access latest --secret=${SECRET_NAME}
```

Set the pw in the DB
```bash
gcloud sql users set-password ${DB_USER} \
--host=% \
--instance=${INSTANCE_NAME} \
--password="$(gcloud secrets versions access latest --secret=${SECRET_NAME})"
```

**I couldn't get it working from console!!**

```bash
gcloud beta run deploy wordpress --region ${REGION}\
    --platform managed --image gcr.io/${GOOGLE_CLOUD_PROJECT}/${IMAGE_NAME}\
    --set-env-vars DB_NAME=$DB_NAME,DB_USER=$DB_USER,DB_HOST=$DB_HOST\
    --port 80\
    --vpc-connector ${VPC_CONNECTOR}\
    --add-cloudsql-instances ${CLOUD_SQL_CONNECTION_NAME}\
    --update-secrets=DB_PASSWORD=${SECRET_NAME}:latest\
    --allow-unauthenticated
```
